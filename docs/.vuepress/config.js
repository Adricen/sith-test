module.exports = {
    title: 'Fablab Ulb ❤️ Fabacademy',
    description: 'Un programme de formation distribué sur 250 fablab dans le monde',
    base: '/',
    // dest: 'public'
    themeConfig: {
      nav: [
        { text: 'Home', link:'/' },
        { text: 'Blog', link: '/blog/' }
      ]
    }
}
